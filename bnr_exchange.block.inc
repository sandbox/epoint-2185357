<?php
/**
 * @file
 * The block related hooks and functions.
 */

/**
 * Called in hook_block_view().
 */
function bnr_exchange_block_view_list_currencies() {
  // Fetching and putting into an "easier to use" array the data containing the
  // checked options set in the administration page of the module.
  // EUR, HUF and USD are the default values to be used. Creating the default
  // array to match bnr_exchange_enabled_currencies structure.
  $admin_currency_map = variable_get('bnr_exchange_enabled_currencies', array(
    'EUR' => array(
      'currency_option' => array('EUR' => 'EUR'),
      'weight' => '0',
    ),
    'HUF' => array(
      'currency_option' => array('HUF' => 'HUF'),
      'weight' => '0',
    ),
    'USD' => array(
      'currency_option' => array('USD' => 'USD'),
      'weight' => '0',
    ),
  ));

  $interested_currencies = array();
  // Selecting only the data needed from the fetched array.
  foreach ($admin_currency_map as $key => $value) {
    if ($value['currency_option'][$key]) {
      $interested_currencies[] = $value['currency_option'][$key];
    }
  }
  // Fetching the cache time option set in the block configuration page.
  $cache_time = variable_get('bnr_exchange_cache_time_set', '3600');
  // Checking if the cached last date it's not expired.
  $cached = cache_get('bnr_exchange:last_date', 'cache');
  if ($cached) {
    $last_exchange_date = $cached->data;
  }
  // Obtaining the last available date from the data base. This date is taken
  // from the parsed file of the currency exchange provider site.
  if (empty($last_exchange_date)) {
    $qdate = db_select('bnr_exchange', 'bx')
    ->fields('bx', array('date'))
    ->orderBy('date', 'DESC')
    ->range(0, 1)
    ->execute()
    ->fetchAssoc();
    $last_exchange_date = $qdate['date'];
    // Saving the last date in the cache bin.
    // config time in seconds.
    cache_set('bnr_exchange:last_date', $last_exchange_date, 'cache', time() + (int) $cache_time);
  }
  // Testing if the block's content data is cached, if true the hook returns
  // this data it already has stored.
  $cached = cache_get('bnr_exchange:block:list_currencies:' . $last_exchange_date, 'cache');
  if ($cached) {
    $content = $cached->data;
    // Creating the block of data containing the theme processed data.
    $block = array(
      'subject' => t('BNR Currencies (:date)', array(
        ':date' => format_date(strtotime($last_exchange_date), 'custom', 'm/d/Y'),
        )
      ),
      'content' => $content,
    );
    return $block;
  }
  $result = array();
  // Obtaining the content data prom the database. This data is taken from the
  // parsed file of the currency exchange provider site.
  if (empty($result)) {
    $result = db_select('bnr_exchange', 'bx')
    ->fields('bx', array('code', 'value', 'multiplier', 'date'))
    ->condition('date', $last_exchange_date, '=')
    ->execute()
    ->fetchAll();
  }
  // Fetching the weights of the currency fields (set in the block
  // configuration page).
  $exchange_weights = variable_get('bnr_exchange_currencies_weight', array());
  // Obtaining the list of fields with the weights attached for sorting.
  $list = array();
  $i = 0;
  // Counter used to view the currency table the right way according to the
  // default weights (in case of use of the default weights).
  $k = 0;
  foreach ($result as $field) {
    if (in_array($field->code, $interested_currencies)) {
      // Time processing functions that are expensive.
      $list[$i][] = $field->multiplier . ' ' . $field->code;
      $list[$i][] = number_format($field->value, 4);
      $list[$i]['weight'] = isset($exchange_weights[$field->code]) ? $exchange_weights[$field->code] : ++$k;
      ++$i;
    }
  }

  // Sorting our created $list array coresponding to weights.
  usort($list, 'drupal_sort_weight');
  // Creating a list from the list of fields, containing only the data needed
  // for the theme and the block view.
  $bnr_list = array();
  foreach ($list as $key => $value) {
    $bnr_list[$key] = array($list[$key][0], $list[$key][1]);
  }
  $header = array(t('Currency'), t('Price in RON'));

  // Gathering all the data together.
  $content = theme('table', array(
    'header' => $header,
    'rows' => $bnr_list,
  ));
  // Creating the block of data containing the theme processed data.
  $block = array(
    'subject' => t('BNR Currencies (:date)', array(
      ':date' => format_date(strtotime($last_exchange_date), 'custom', 'm/d/Y'),
      )
    ),
    'content' => $content,
  );
  // Set block content cache for the cache set value
  // in the block configuration page.
  cache_set('bnr_exchange:block:list_currencies:' . $last_exchange_date, $content, 'cache', $_SERVER['REQUEST_TIME'] + (int) $cache_time);
  return $block;
}

/**
 * Called hook_block_configure().
 */
function bnr_exchange_block_configure_list_currencies() {
  $form = array();
  // Time options in seconds with reading-friendly string pairs.
  $time_set = array(
    '900' => '15 minutes',
    '1800' => '30 minutes',
    '2700' => '45 minutes',
    '3600' => '1 hour',
    '7200' => '2 hours',
    '10800' => '3 hours',
    '14400' => '4 hours',
    '18000' => '5 hours',
    '21600' => '6 hours',
  );
  // The cache time select field in the block configuration page.
  $form['bnr_exchange_cache_time_set'] = array(
    '#type' => 'select',
    '#title' => t('Specify the time interval for BNR Exchange block caching:'),
    '#default_value' => variable_get('bnr_exchange_cache_time_set', '3600'),
    '#description' => t('This allows you to choose the time interval your BNR Exchange block data is cached.'),
    '#options' => $time_set,
    '#multiple' => FALSE,
  );
  // Fetching and putting into an "easier to use" array the data containing the
  // checked options set in the administration page of the module.
  // EUR, HUF and USD are the default values to be used. Creating the default
  // array to match bnr_exchange_enabled_currencies structure.
  $admin_currency_map = variable_get('bnr_exchange_enabled_currencies', array(
    'EUR' => array(
      'currency_option' => array('EUR' => 'EUR'),
      'weight' => '0',
    ),
    'HUF' => array(
      'currency_option' => array('HUF' => 'HUF'),
      'weight' => '0',
    ),
    'USD' => array(
      'currency_option' => array('USD' => 'USD'),
      'weight' => '0',
    ),
  ));

  // $interested_currencies is an array containing the values (code names) of
  // the checked or default exchange currencies.
  $interested_currencies = array();
  // Selecting only the data needed from the fetched array.
  foreach ($admin_currency_map as $key => $value) {
    if ($value['currency_option'][$key]) {
      $interested_currencies[] = $value['currency_option'][$key];
    }
  }
  // The form tree root in 'currency_weight'. Needed for theming the block
  // configure page with tabledrag.
  $form['currency_weight']['#tree'] = TRUE;
  // Fetching the weight of the currency exchange fields set in the block
  // configuration page,...
  $exchange_weights = variable_get('bnr_exchange_currencies_weight', array());
  // ...unsetting the one's that are no more checked in the administration page.
  foreach ($exchange_weights as $key => $val) {
    if (!in_array($key, $interested_currencies)) {
      unset($exchange_weights[$key]);
    }
  }
  // ...and sorting them.
  if ($exchange_weights) {
    asort($exchange_weights);
  }
  // Corelating weights with checked options.
  foreach ($interested_currencies as $val) {
    if (!isset($exchange_weights[$val])) {
      $exchange_weights[$val] = 0;
    }
  }
  // Counter used to list the drag-and-drop list the right way according to the
  // default weights (in case they are not set).
  $k = 0;
  // Creating the form which will be themed as a drag-and-drop with tabledrag in
  // the block configuration page.
  foreach ($exchange_weights as $key => $value) {
    $form['currency_weight'][$key] = array(
      'name' => array(
        '#markup' => check_plain($key),
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => 'Weight for ' . $key,
        '#default_value' => isset($exchange_weights[$key]) ? $exchange_weights[$key] : ++$k,
        '#delta' => 50,
        '#description' => t('This allows you to drag-and-drop the fields of the currency table to set the order in which they will show in the block.'),
        '#title_display' => 'invisible',
      ),
    );
  }
  // Setting the theme for the related hook.
  $form['#theme'] = 'bnr_exchange_block_configure';
  return $form;
}

/**
 * Called in hook_block_save().
 */
function bnr_exchange_block_save_list_currencies($edit) {
  // Saving the cache times in the database.
  variable_set('bnr_exchange_cache_time_set',
  $edit['bnr_exchange_cache_time_set']);
  // Fetching and putting into an "easier to use" array the data containing the
  // checked options set in the administration page of the module.
  // EUR, HUF and USD are the default values to be used. Creating the default
  // array to match bnr_exchange_enabled_currencies structure.
  $admin_currency_map = variable_get('bnr_exchange_enabled_currencies', array(
    'EUR' => array(
      'currency_option' => array('EUR' => 'EUR'),
      'weight' => '0',
    ),
    'HUF' => array(
      'currency_option' => array('HUF' => 'HUF'),
      'weight' => '0',
    ),
    'USD' => array(
      'currency_option' => array('USD' => 'USD'),
      'weight' => '0',
    ),
  ));

  $interested_currencies = array();
  // Selecting only the data needed from the fetched array.
  foreach ($admin_currency_map as $key => $value) {
    if ($value['currency_option'][$key]) {
      $interested_currencies[] = $value['currency_option'][$key];
    }
  }
  $exchange_weights = array();
  foreach ($interested_currencies as $key => $value) {
    $exchange_weights[$value] = $edit['currency_weight'][$value]['weight'];
  }
  // Setting the block configuration elements' data and weights in their
  // proccessesd state.
  variable_set('bnr_exchange_currencies_weight', $exchange_weights);

  // Clearing all bnr_exchange related caches for refreshing the changed values.
  cache_clear_all('bnr_exchange', 'cache', TRUE);
}
