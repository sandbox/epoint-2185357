<?php
/**
 * @file
 * Administration page callbacks for the bnr_exchange module.
 */

/**
 * Define a form for configuration settings on admin page.
 */
function bnr_exchange_admin_settings() {
  $codes = variable_get('bnr_exchange_currencies_codes', array(
    'EUR' => 'EUR',
    'USD' => 'USD',
    'HUF' => 'HUF',
    )
  );
  // The text field for inserting the url of the xml file containing
  // the exchange currency course.
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Valid URL for XML file'),
    '#default_value' => variable_get('bnr_exchange_rates_xml_url', BNR_EXCHANGE_DEFAULT_BNR_URL),
  );
  // $interested_currencies is an array containing the the currencie and their
  // checked/unchecked values as data.
  $interested_currencies = variable_get(
    'bnr_exchange_enabled_currencies', array()
  );
  $array_weight_currency = array();
  foreach ($interested_currencies as $key => $value) {
    // Create an array with currency keys and weights values.
    $array_weight_currency[$key] = $value['weight'];
  }
  // Sort the new array after weight values.
  asort($array_weight_currency, SORT_NUMERIC);
  $weight_sorted_currency = array();
  foreach ($array_weight_currency as $key => $value) {
    // Rearrange the ordored array as the initial array structure.
    $weight_sorted_currency[$key] = $key;
  }
  foreach ($codes as $key => $value) {
    // Rearrange the ordored array as the initial array structure.
    $weight_sorted_currency[$key] = $value;
  }
  // The form tree root in 'code'. Needed for theming the block configure page
  // with tabledrag.
  $form['code']['#tree'] = TRUE;
  // Counter used to list the drag-and-drop list the right way according to the
  // default weights (in case they are not set).
  $k = 0;
  foreach ($weight_sorted_currency as $key => $value) {
    $form['code'][$key] = array(
      'currency_option' => array(
        '#type' => 'checkboxes',
        '#options' => array($key => $value),
        '#default_value' => array(isset($interested_currencies[$key]['currency_option'][$key]) ? $interested_currencies[$key]['currency_option'][$key] : 0),
      ),
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => isset($interested_currencies[$key]['weight']) ? $interested_currencies[$key]['weight'] : ++$k,
        '#delta' => 50,
        '#title_display' => 'invisible',
      ),
    );
  }
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('bnr_exchange_admin_settings_update_submit'),
  );
  $form['save_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array(
      'bnr_exchange_admin_settings_interested_currencies',
    ),
    '#suffix' => t('Press Update to fetch currencies and values from BNR server and press Save to save configuration'),
  );
  $form['#theme'] = 'bnr_exchange_admin_settings';
  return $form;
}

/**
 * Theme function for the available table form.
 */
function theme_bnr_exchange_admin_settings($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['example_items'] array.
  foreach (element_children($form['code']) as $id) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    //
    // This could also have been done during the form declaration by adding
    // '#attributes' => array('class' => 'example-item-weight'),
    // directy to the 'weight' element in tabledrag_example_simple_form().
    $form['code'][$id]['weight']['#attributes']['class'] = array(
      'example-item-weight',
    );
    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.
    $rows[] = array(
      'data' => array(
        // Add our 'description' column.
        drupal_render($form['code'][$id]['currency_option']),
        // Add our 'weight' column.
        drupal_render($form['code'][$id]['weight']),
      ),
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'. This will add the 'draggable'
      // class to the <tr> element for that row when the final table is
      // rendered.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  $header = array(t('Available currencies'), t('Weight'));

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  // Because an element's 'id' should be unique on a page, make sure the value
  // you select is NOT the same as the form ID used in your form declaration.
  $table_id = 'example-items-table';

  // We can render our tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements (such as our submit button).
  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function in order to add the
  // tabledrag.js goodness onto our page.
  //
  // For a basic sortable table, we need to pass it:
  // - the $table_id of our <table> element,
  // - the $action to be performed on our form items ('order'),
  // - a string describing where $action should be applied ('siblings'),
  // - and the class of the element containing our 'weight' element.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'example-item-weight');

  return $output;
}


/**
 * Process bnr_exchange settings submission.
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form values.
 */
function bnr_exchange_admin_settings_interested_currencies($form, &$form_state) {
  $new_codes = $form_state['values']['code'];
  // Saving the checked/unchecked values into bnr_exchange_enabled_currencies
  // system variable.
  variable_set('bnr_exchange_enabled_currencies', $new_codes);
  // Take the value from text field and set to a system variable.
  if (!empty($form_state['values']['path'])) {
    variable_set('bnr_exchange_rates_xml_url', $form_state['values']['path']);
    drupal_set_message(t('The configuration options have been saved.'));
  }
  // Clearing all bnr_exchange related caches for refreshing the changed values.
  cache_clear_all('bnr_exchange', 'cache', TRUE);
}

/**
 * Process update settings submission.
 */
function bnr_exchange_admin_settings_update_submit($form, $form_state) {
  module_load_include('inc', 'bnr_exchange', 'bnr_exchange.helper');
  // Helper function for fetching and parsing the data from the currency
  // exchange provider residing in bnr_exchange.module file.
  bnr_exchange_data_extraction();
}

/**
 * Validate bnr_exchange settings submission.
 */
function bnr_exchange_admin_settings_validate($form, &$form_state) {
  // Checks if the XML file is valid.
  if (!bnr_exchange_is_valid_xml_structure($form_state['values']['path'])) {
    form_set_error('path', t('The file is not a valid xml'));
  }
}

/**
 * XML file's structure validation.
 *
 * @param string $file
 *   The xml file to check.
 *
 * @return bool
 *   True if file is XML valid and false if not.
 */
function bnr_exchange_is_valid_xml_structure($file) {
  $prev = libxml_use_internal_errors(TRUE);
  $ret = TRUE;
  try {
    @new SimpleXMLElement($file, 0, TRUE);
  }
  catch (Exception $e) {
    $ret = FALSE;
  }
  if (count(libxml_get_errors()) > 0) {
    // There have been XML errors.
    $ret = FALSE;
  }
  // Tidy up.
  libxml_clear_errors();
  libxml_use_internal_errors($prev);
  return $ret;
}
