<?php
/**
 * @file
 * Includes a function used in cron, administration updates and instalation.
 */

/**
 * Helper function to extract data from xml file.
 *
 * This method is used for fetching and parsing the data
 * from the currency exchange provider.
 */
function bnr_exchange_data_extraction() {
  // Get contents of an XML file and fetch the data in variables and arrays.
  $url = variable_get('bnr_exchange_rates_xml_url', BNR_EXCHANGE_DEFAULT_BNR_URL);
  $file = @file_get_contents($url);
  if ($file != '') {
    // Creating an object with the xml parsed data.
    $xml = new SimpleXMLElement($file);
    $codes = array();
    $currencies = $xml->Body->Cube->Rate;
    $date = (string) $xml->Header->PublishingDate;
    $multiplier = array();
    $count = 0;
    foreach ($currencies as $value) {
      $code = $value->attributes()->currency->__toString();
      $codes[$code] = $code;
      $exchange_value = (string) $value;
      $multiplier = max(1, (int) $value->attributes()->multiplier);
      // Insert or update records to bnr_exchange database.
      $status = db_merge('bnr_exchange')
      ->key(
        array(
          'code' => $code,
          'date' => $date,
        )
      )
      ->insertFields(
        array(
          'code' => $code,
          'value' => $exchange_value,
          'multiplier' => $multiplier,
          'date' => $date,
        )
      )
      ->updateFields(
        array(
          'value' => $exchange_value,
          'multiplier' => $multiplier,
          'changed' => time(),
        )
      )
      ->execute();
      // Check if query's result if is a failure.
      if (!($status == 1 || $status == 2)) {
        ++$count;
      }
    }
    if ($count > 0) {
      drupal_set_message(
        t('The BNR exchange rates were updated with failure!'),
        'error'
      );
    }
    else {
      drupal_set_message(
        t('The BNR exchange rates have been successfully updated!'));
    }
    variable_set('bnr_exchange_currencies_codes', $codes);
    cache_clear_all('bnr_exchange', 'cache', TRUE);
  }
  else {
    drupal_set_message(t('The file could not be parsed'), 'error');
  }
}
