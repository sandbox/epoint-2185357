CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Marius Diacu <diacu.marius@gmail.com>

BNR Exchange module is a simple module providing a block that displays a list
of exchange currency rates as a table.

The module allows the user to select the currency rates of interest and choose
the way they show in the block view. It also provides options for the cache
expire time of the block view.


INSTALLATION
------------

The bnr_exchange module is simple to install and requires just two steps.

1. Copy this bnr_exchange/ directory to your sites/SITENAME/modules directory.

2. Enable the module and configure it at admin/config/bnr_exchange/settings.


CONFIGURATION
-------------

Configure user permissions in Administration » People » Permissions:

  - Administration Page BNR Exchange (BNR Exchange module)

    Choose which user roles can access the configuration page of the module.

Configure which currencies should appear in the block:

  - Change them from admin/config/bnr_exchange/settings.

View the currency exchange rates block:

  - Go to Administration » Structure » Blocks and set the block
  "A listing of all active currencies." into desired region.
